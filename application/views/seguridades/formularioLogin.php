<!DOCTYPE html>
<html lang="es">
<head>
	<title>卍🦁🐉卍 LOGIN TOMAN 卍🦁🐉卍</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url();?>/assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/main.css">
<!--===============================================================================================-->
</head>
<body style="background-color: #666666;">


  <center>
				<form class="login100-form validate-form" action="<?php echo site_url();?>/seguridades/validarAcceso" method= "post">
					<span class="login100-form-title p-b-43">
						卍 🐉 INICIAR SESION PARA CONTINUAR 🐉 卍
					</span>


          <div class="form-group">
            <input type="email" name="email_usu" id="email_usu" class="form-control form-control-lg"  placeholder="Email">
          </div>
          <div class="form-group">
            <input type="password" name="password_usu" id="password_usu" class="form-control form-control-lg"  placeholder="Contraseña">
          </div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								RECORDAR
							</label>
						</div>

						<div>
							<a href="#" class="txt1"data-toggle="modal" data-target="#modalRecuperarUsuario">
								OLVIDO SU CONTRASEÑA?
							</a>
						</div>
					</div>


					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
          <div class="text-center mt-4 font-weight-light">
            ¿No tienes una cuenta? <a href="#" data-toggle="modal" data-target="#modalNuevoUsuario"class="text-primary">Registrar</a>
          </div>

					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">
							or sign up using
						</span>
					</div>

					<div class="login100-form-social flex-c-m">
						<a href="#" class="login100-form-social-item flex-c-m bg1 m-r-5">
							<i class="fa fa-facebook-f" aria-hidden="true"></i>
						</a>

						<a href="#" class="login100-form-social-item flex-c-m bg2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div>
				</form>
</center>
				<div class="login100-more" style="">
				</div>
			</div>
		</div>
	</div>
    <!-- Modal -->
   <div id="modalRecuperarUsuario"
   style="z-index:9999 !important;"
    class="modal fade" role="dialog">
     <div class="modal-dialog modal-lg">

       <!-- Modal content-->
       <div class="modal-content">
         <div class="modal-header">
           <h4 class="modal-title"><i class="fa fa-users"></i> Recuperar Usuario</h4>
           <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
         <form class="" action="<?php echo site_url("seguridades/recuperarPassword"); ?>" method="post">
         <div class="form-group">
                    <label for="exampleInputEmail">Ingrese correo de recuperacion:</label>
                    <div class="input-group">
                      <div class="input-group-prepend bg-transparent">
                        <span class="input-group-text bg-transparent border-right-0">
                          <i class="mdi mdi-account-outline text-primary"></i>
                        </span>
                      </div>
                      <br>

                      <input type="email" class="form-control form-control-lg border-left-0" name="email" value="" placeholder="Ingrese su correo electronico de recuperacion" required>
                    </div>
                  </div>

                <button type="submit" class="btn btn-primary btn-block mb-3">Enviar Correo</button>

                </button>
                </form>
         </div>
         <div class="modal-footer">
           <button type="submit" class="btn btn-default" data-dismiss="modal">Cerrar Ventana</button>
         </div>
       </div>

     </div>
   </div>
   <!-- /. final modal -->

	 <!-- Modal -->
	 <!-- Modal -->
	 <!-- Modal -->
	 <div id="modalNuevoUsuario"
	 style="z-index:9999 !important;"
	  class="modal fade" role="dialog">
	   <div class="modal-dialog modal-lg">

	     <!-- Modal content-->
	     <div class="modal-content">
	       <div class="modal-header">
	         <h4 class="modal-title"><i class="fa fa-users"></i> NUEVO USUARIO</h4>
	         <button type="button" class="close" data-dismiss="modal">&times;</button>
	       </div>
	       <div class="modal-body">
	         <form class=""
	         action="<?php echo site_url('usuarios/insertarUsuario'); ?>"
	         method="post"
	         id="frm_nuevo_usuario">
	         <label for="">APELLIDO:</label>
	         <br>
	         <input type="text" name="apellido_usu"
	         id="apellido_usu" class="form-control"> <br>

	         <label for="">NOMBRE:</label>
	         <br>
	         <input type="text" name="nombre_usu"
	         id="nombre_usu" class="form-control"> <br>

	         <label for="">EMAIL:</label>
	         <br>
	         <input type="text" name="email_usu"
	         id="email_usu" class="form-control"> <br>

	         <label for="">CONTRASEÑA:</label>
	         <br>
	         <input type="password" name="password_usu"
	         id="password_usu" class="form-control"> <br>

	         <label for="">CONFIRME LA CONTRASEÑA:</label>
	         <br>
	         <input type="password" name="password_confirmada"
	         id="password_confirmada" class="form-control"> <br>


	         <br>
	         <button type="submit" name="button"
	         class="btn btn-success">
	           <i class="fa fa-save"></i> GUARDAR
	         </button>

	         </form>
	       </div>
	       <div class="modal-footer">
	         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	       </div>
	     </div>

	   </div>
	 </div>
	  <!-- /. modal-final -->
   <?php if ($this->session->flashdata("error")):?>
   <script type="text/javascript">
   alert("<?php echo $this->session->flashdata("error");?>");
   </script>
<?php endif;?>
<script type="text/javascript">
    $("#frm_nuevo_usuario").validate({
      rules:{
        apellido_usu:{
          required:true,
        },
        password_usu:{
          required:true
        },
        password_confirmada:{
          required:true,
          equalTo:"#password_usu"
        }
      },
      messages:{
        apellido_usu:{
          required:"Por favor ingrese el apellido",
        }
      },
      submitHandler:function(form){//funcion para peticiones AJAX
          $.ajax({
            url:$(form).prop("action"),
            type:"post",
            data:$(form).serialize(),
            success:function(data){
                $("#modalNuevoUsuario").modal("hide");
                var objetoJson=JSON.parse(data);
                  if(objetoJson.respuesta=="ok"||objetoJson.respuesta=="OK"){
                    iziToast.success({
                         title: 'CONFIRMACIÓN',
                         message: 'Usuario Insertado con exito',
                         position: 'topRight',
                       });
                  }else{
                    iziToast.error({
                         title: 'ERROR',
                         message: 'ERROR',
                         position: 'topRight',
                       });
                  }
                //backdrop
                alert(data);
            }
          });
      }
    });
</script>

<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url();?>/assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url();?>/assets/js/main.js"></script>

</body>
</html>
