
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

<!-- partial:partials/_footer.html -->
<footer class="footer">
  <div class="d-sm-flex justify-content-center justify-content-sm-between">
    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin template</a> from BootstrapDash.</span>
    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2021. All rights reserved.</span>
  </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="<?php echo base_url();?>/assets/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="<?php echo base_url();?>/assets/vendors/chart.js/Chart.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url();?>/assets/vendors/progressbar.js/progressbar.min.js"></script>

<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="<?php echo base_url();?>/assets/js/off-canvas.js"></script>
<script src="<?php echo base_url();?>/assets/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url();?>/assets/js/template.js"></script>
<script src="<?php echo base_url();?>/assets/js/settings.js"></script>
<script src="<?php echo base_url();?>/assets/js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo base_url();?>/assets/js/dashboard.js"></script>
<script src="<?php echo base_url();?>/assets/js/Chart.roundedBarCharts.js"></script>
<!-- End custom js for this page-->
<!-- ===============================================-->
    <!--    INICIO-->
    <!-- ===============================================-->
    <!-- jquery deley para que se vea bonito -->

	<?php if ($this->session->flashdata('confirmacion')): ?>
  <script type="text/javascript">
    iziToast.info({
  tittle: 'CONFIRMACION',
  message: '<?php echo $this->session->flashdata('confirmacion'); ?>',
  position: 'topRight',
});
</script>
<?php endif; ?>


<?php if ($this->session->flashdata('error')): ?>
  <script type="text/javascript">
    iziToast.danger({
  tittle: 'ADVERTENCIA',
  message: '<?php echo $this->session->flashdata('error'); ?>',
  position: 'topRight',
});
</script>
<?php endif; ?>

<style media="screen">
  .error{
    color:red;
    font-size: 16px;

  }
  input.error, select.error{
    border: 2px solid red;
  }
</style>

<?php if ($this->session->flashdata("bienvenida")): ?>
  <script type="text/javascript">
    iziToast.info({
         title: 'CONFIRMACIÓN',
         message: '<?php echo $this->session->flashdata("bienvenida"); ?>',
         position: 'topRight',
       });
  </script>
<?php endif; ?>



    <!-- ===============================================-->
    <!--    FIN-->
    <!-- ===============================================-->
</body>

</html>
